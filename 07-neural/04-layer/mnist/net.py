import numpy as np

class Node:
    def __init__(self, v = 0, g = 0):
        self.v = v # 輸出值 (f(x))
        self.g = g # 梯度值 (偏微分)

    def __str__(self):
        return 'v:{self.v} g:{self.g}'.format(self=self)

class Layer:
    def __init__(self, o, x, y):
        self.o = o
        self.x = x
        self.y = y

    def forward(self):
        self.o.v = self.f(self.x.v, self.y.v)
        return self.o.v

    def backward(self):
        x, y, o, gfx, gfy = self.x, self.y, self.o, self.gfx, self.gfy
        x.g += gfx(x.v,y.v) * o.g
        y.g += gfy(x.v,y.v) * o.g

    def adjust(self, step=0.001): # 朝逆梯度的方向走一小步
        x, y = self.x, self.y
        x.v -= step*x.g
        y.v -= step*y.g
        x.g = 0
        y.g = 0

class FullyLayer:
    def __init__(self, o, x, w):
        self.o = o
        self.x = x
        self.w = w

    def forward(self):
        assert x.shape[1] == W.shape[0]
        return x.dot(W) + b
        self.o.v = self.f(self.x.v, self.y.v)
        return self.o.v

    def backward(self):
        x, y, o, gfx, gfy = self.x, self.y, self.o, self.gfx, self.gfy
        x.g += gfx(x.v,y.v) * o.g
        y.g += gfy(x.v,y.v) * o.g

    def adjust(self, step=0.001): # 朝逆梯度的方向走一小步
        x, y = self.x, self.y
        x.v -= step*x.g
        y.v -= step*y.g
        x.g = 0
        y.g = 0

'''
FullyConnLayer.prototype = {
  forward: function(V, is_training) {
    this.in_act = V;
    var A = new Vol(1, 1, this.out_depth, 0.0);
    var Vw = V.w;
    for(var i=0;i<this.out_depth;i++) {
      var a = 0.0;
      var wi = this.filters[i].w;
      // 計算全部互聯的加總值
      for(var d=0;d<this.num_inputs;d++) {
        a += Vw[d] * wi[d]; // for efficiency use Vols directly for now
      }
      a += this.biases.w[i]; // 再加上 bias ，得到輸出結果
      A.w[i] = a;
    }
    this.out_act = A;
    return this.out_act;
  },
  backward: function() {
    var V = this.in_act;
    V.dw = Util.zeros(V.w.length); // zero out the gradient in input Vol
    
    // compute gradient wrt weights and data
    for(var i=0;i<this.out_depth;i++) {
      var tfi = this.filters[i];
      var chain_grad = this.out_act.dw[i];
      for(var d=0;d<this.num_inputs;d++) {
        V.dw[d] += tfi.w[d]*chain_grad; // grad wrt input data
        tfi.dw[d] += V.w[d]*chain_grad; // grad wrt params
      }
      this.biases.dw[i] += chain_grad;
    }
  },

def fully(x, W, b):
    assert x.shape[1] == W.shape[0]
    return x.dot(W) + b

def backwardFully(g, a):
    DW = np.dot(g.T, a).T
    db = np.sum(g, axis = 0)
    return (DW, db)

def update(W, b, lr, DW, db):
    assert DW.shape == W.shape
    W -= lr * DW
    assert db.shape == b.shape
    b -= lr * db
'''

class Net:
    def __init__ (self):
        self.layers = []

    def forward(self): # 正向傳遞計算結果
        for layer in self.layers:
            layer.forward()
        return self.o.v

    def backward(self): # 反向傳遞計算梯度 
        self.o.g = 1 # 設定輸出節點 o 的梯度為 1
        for layer in reversed(self.layers): # 反向傳遞計算每個節點 Node 的梯度 g
            layers.backward()

    def adjust(self, step=0.01): # 朝逆梯度的方向走一小步
        for layer in self.layers:
            layer.adjust(step)

    # 使用 forward-backward 的方式計算梯度的梯度下降法
    def gradient_descendent (self, maxLoops=100, dumpPeriod=1, step=0.01):
        for loop in range(maxLoops):
            energy = self.forward()
            if loop % dumpPeriod==0:
                print(loop, ' => ', energy)
            self.backward()
            self.adjust(step)
