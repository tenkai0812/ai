import numpy as np
import matplotlib.pyplot as plt

def load_data(path):
    def one_hot(y):
        table = np.zeros((y.shape[0], 10))
        for i in range(y.shape[0]):
            table[i][int(y[i][0])] = 1 
        return table

    def normalize(x): 
        x = x / 255
        return x 

    data = np.loadtxt('{}'.format(path), delimiter = ',')
    return normalize(data[:,1:]),one_hot(data[:,:1])

print("start()")
print('load data....')
X_train, y_train = load_data('_data/train.csv')
X_test, y_test = load_data('_data/test.csv')

'''
NN = NeuralNetwork(X_train, y_train) 
print("train()")
NN.train()
print("plot()")
NN.plot()
NN.test(X_test,y_test)
'''
