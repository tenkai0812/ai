# Attention 注意力機制

## Self Attention

* https://youtu.be/hYdO9CscNes?t=1330

輸入: a[1..n], 輸出: b[1..n]

```
a*Wq = q    (query 問題)
a*Wk = k    (key 權重)
a*Wv = v    (value 值)

qi*kj = attention_score(qi,kj)=alpha[i,j]

alpha'[i,j] = softmax(alpha[i,j])

v*alpha = b

```

## Masked Self Attention

不能往後看，只能往前看

* https://youtu.be/N6aRv06iv2g?t=685
